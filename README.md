Projeto de implementação de script bash para gerar backups no formato tarball

Este programa pode ser rodado tanto da linha de comando quanto via crontab


Referências:
  
* https://devhints.io/bash
* https://crontab.guru/
* https://www.tutorialspoint.com/unix/unix-io-redirections.htm

Como rodar:
  
    chmod +x autobackups
    ./autobackups

Crontab:
  
    crontab -e
    # ou
    crontab <arquivo.crontab>